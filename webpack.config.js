const path = require('path')

module.exports = {
    entry: './src/index.ts',
    output: {
        filename: 'kalop.js',
        path: path.resolve(__dirname, 'dist'),
        library: 'KalopKernel',
        libraryTarget: 'umd',
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                use: 'babel-loader',
            },
        ],
    },
    resolve: {
        extensions: ['.ts', '.js'],
    }
}
