import { IClientRoot, IManifest, IPlugin } from '../types'

export class ClientRootFactory implements IClientRoot {
  manifest: IManifest = { key: '', name: '' }
  $extension: { [module: string]: IPlugin | undefined; } = {}

  useExtension (moduleInstance: IPlugin): void {
    this.$extension[moduleInstance.manifest.key] = moduleInstance
  }

  setManifest (manifest: IManifest): void {
    this.manifest = manifest
  }
}

const rootManifest:IManifest = {
  key: 'maestro',
  name: 'Maestro'
}

const app = new ClientRootFactory()
app.setManifest(rootManifest)

export { app }
