import { IPlugin, IPluginManager } from '../../types'

export class PluginManager implements IPluginManager {
  plugins: IPlugin[] = [];

  constructor () {
    console.log('PluginManager created')
  }

  run (): void {
    this.plugins.forEach(plugin => {
      console.log('Plugin started:', plugin.manifest.name)
      plugin.onInstall()
      plugin.onLoad()
    })
  }

  use (plugin: IPlugin): void {
    console.log('Plugin added:', plugin.manifest.name)
    this.plugins.push(plugin)
  }
}
