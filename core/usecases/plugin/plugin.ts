import { IPlugin, IManifest } from '../../types/index'

class Plugin implements IPlugin {
  manifest: IManifest

  constructor ({ key, name }: IManifest) {
    this.manifest = { key, name }
  }

  onUninstall (): void {
    console.log('onUninstall')
  }

  onInstall (): void {
    console.log('onInstall')
  }

  onLoad (): void {
    console.log('onLoad')
  }

  onUnload (): void {
    console.log('onUnload')
  }
}

const createPlugin = ({ key, name }: IManifest) => {
  return new Plugin({ key, name })
}
export { createPlugin }
