import { IClientRoot, IClient, IManifest, IPlugin, IPluginManager } from '../../types'
import { PluginManager } from '../plugin/plugin-manager'

class CreateClient implements IClient {
  manifest: IManifest = { key: '', name: '' }
  $root: { [root: string]: IClientRoot } = {};
  $extension: { [module: string]: IPlugin | undefined } = {}
  private $pluginManager: IPluginManager

  constructor () {
    this.$pluginManager = new PluginManager()
  }

  useExtension (extensionInstance: IPlugin): void {
    this.$extension[extensionInstance.manifest.key] = extensionInstance
    this.$pluginManager.use(extensionInstance)
    this.$pluginManager.run()
  }

  useRoot (rootInstance: IClientRoot): void {
    this.$root[rootInstance.manifest.key] = rootInstance
  }

  setManifest (manifest: IManifest): void {
    this.manifest = manifest
  }
}

const createClient = new CreateClient()
export { createClient }
