import { IManifest } from '../../types/Manifest'

export class SetManifest {
  manifest: IManifest

  constructor (manifest: IManifest) {
    this.manifest = manifest
  }

  execute (): void {
    console.log(this.manifest)
  }
}
