export interface IManifest {
  key: string
  name: string
  description?: string
  repository?: string
}
