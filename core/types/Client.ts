import { IPlugin, IManifest } from './index'

interface IClientBase {
  manifest: IManifest
  $extension: { [module: string]: IPlugin | undefined }
  useExtension(extensionInstance: IPlugin): void
  setManifest(manifest: IManifest): void
}

export type IClientRoot = IClientBase

export interface IClient extends IClientBase {
  $root: { [root: string]: IClientRoot } | undefined;
  useRoot(rootInstance: IClientRoot): void
}
