import { IManifest } from './Manifest'

export interface IPlugin {
  manifest: IManifest
  onUninstall(): void
  onInstall(): void
  onLoad(): void
  onUnload(): void
}

export interface IPluginManager {
  plugins: IPlugin[]
  use(plugin: IPlugin): void;
  run(): void;
}
