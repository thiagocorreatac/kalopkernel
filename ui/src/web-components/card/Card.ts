import { LitElement, html } from 'lit'

export class Card extends LitElement {
  constructor () {
    super()
    console.log('Card in use!')
  }

  static get properties () {
    return { title: { type: String } }
  }

  render () {
    return html`
      <div class="card">
        <div slot="header"><p>Sample Header</p></div>
        <div>
          <p>Sample content ${this.title}</p>
        </div>
        <slot></slot>
      </div>
    `
  }
}

customElements.define('kalop-card', Card)
